package main

import uuid "github.com/nu7hatch/gouuid"

// App is the app's mode manager
type App interface {
	// Connect will create a new connection
	Connect(uint32, string) error
	// Disconnect will disconnect the connection or notify about it
	Disconnect(uint32) error
	// Data transfers the data from the other side
	Data(uint32, []byte) error

	MyID() *uuid.UUID
	OtherID() *uuid.UUID
	SetOther(*uuid.UUID)
}
