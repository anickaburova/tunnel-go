package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type AWSTunnel struct {
	fileIn  string
	fileOut string
	wait    time.Duration
	comm    *s3.S3
	bucket  string
}

func CreateAWS(fileIn, fileOut string) (result *AWSTunnel, err error) {
	awsRegion := os.Getenv("AWS_REGION")
	bucket := os.Getenv("AWS_BUCKET")
	fmt.Println("region: ", awsRegion)
	fmt.Println("bucket: ", bucket)
	session, err := session.NewSession(&aws.Config{Region: aws.String(awsRegion)})
	s3 := s3.New(session)
	if err != nil {
		return
	}
	log.Println("AWS session created")
	_, err = session.Config.Credentials.Get()
	if err != nil {
		return
	}
	log.Println("AWS session connected")
	tunnel := AWSTunnel{
		fileIn:  fileIn,
		fileOut: fileOut,
		wait:    time.Millisecond * 2000,
		comm:    s3,
		bucket:  bucket,
	}
	result = &tunnel
	return
}

func (tunnel *AWSTunnel) Send(data []byte) error {
	log.Printf("AWS tunnel is sending %d bytes", len(data))
	input := &s3.PutObjectInput{
		Body:   aws.ReadSeekCloser(bytes.NewReader(data)),
		Bucket: aws.String(tunnel.bucket),
		Key:    aws.String(tunnel.fileOut),
	}
	_, err := tunnel.comm.PutObject(input)
	return err
}

func (tunnel *AWSTunnel) SetCallback(callback TunnelCallback) {
	go func() {
		log.Println("AWS tunnel started input loop")
		input := &s3.GetObjectInput{
			Bucket: aws.String(tunnel.bucket),
			Key:    aws.String(tunnel.fileIn),
		}
		report := 0
		var lastTime time.Time
		for {
			func() {
				result, err := tunnel.comm.GetObject(input)
				if err != nil {
					if aerr, ok := err.(awserr.Error); ok {
						if aerr.Code() == s3.ErrCodeNoSuchKey {
							if report != 1 {
								report = 1
								log.Println(aerr)
							}
							goto wait
						}
					}
					log.Fatal(err)
				} else {
					defer result.Body.Close()
					if thisTime := *result.LastModified; lastTime.Before(thisTime) {
						l := *result.ContentLength
						data := make([]byte, l)
						n, err := result.Body.Read(data)
						if err != nil && int64(n) != l {
							goto wait
						}
						lastTime = thisTime
						callback(data)
					} else {
						if report != 2 {
							report = 2
							log.Printf("Times are: %v and %v", thisTime, lastTime)
						}
					}
				}
			wait:
				time.Sleep(tunnel.wait)
			}()
		}
	}()
}
