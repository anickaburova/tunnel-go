package main

import "net"

func CreateClient(connectionID uint32, address string, msgQueue chan Msg) (connection *Connection, err error) {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return
	}
	connection = CreateConnection(address, conn, msgQueue, &connectionID)
	return
}
