package main

import (
	"log"
	"net"
)

// Connection is the connection to some address

type Connection struct {
	id           uint32
	conn         net.Conn
	onDisconnect []DisconnectMonitor
	disconnected bool
	msgQueue     chan Msg
	address      string
}

func CreateConnection(address string, conn net.Conn, msgQueue chan Msg, id *uint32) *Connection {
	log.Println("Creating connection...")
	var connection Connection
	if id == nil {
		connection.id = GetAvailableConnectionID()
	} else {
		connection.id = *id
	}
	msgQueue <- &MsgConnect{connection.id, address}
	connection.disconnected = false
	connection.conn = conn
	connection.address = address

	go func(conn *Connection) {
		log.Printf("Connection %d loop started", conn.id)
		for {
			buffer := make([]byte, 2048)
			n, err := conn.conn.Read(buffer)
			if err != nil {
				log.Printf("Connection %d error: %v", conn.id, err)
				break
			}
			if n > 0 {
				log.Printf("Connection %d received %d bytes: '%s'", conn.id, n, string(buffer[:n]))
				msgQueue <- &MsgData{conn.id, buffer[:n]}
			} else {
				log.Printf("Connection %d received zero data", conn.id)
			}
		}
		log.Printf("Connection %d loop ended, finishing", conn.id)
		if !conn.disconnected {
			msgQueue <- &MsgDisconnect{conn.id}
			for _, dm := range conn.onDisconnect {
				dm.ReportDisconnect(conn.id)
			}
		}
	}(&connection)

	return &connection
}

func (conn *Connection) AddDisconnectMonitor(dm DisconnectMonitor) {
	conn.onDisconnect = append(conn.onDisconnect, dm)
}

func (conn *Connection) ID() uint32 {
	return conn.id
}

// Disconnect will disconnect the connection or notify about it
func (conn *Connection) Disconnect() error {
	log.Printf("Connection %d received disconnect message", conn.id)
	conn.disconnected = true
	conn.conn.Close()
	return nil
}

// Data transfers the data from the other side
func (conn *Connection) Data(data []byte) error {
	l, err := conn.conn.Write(data)
	if err != nil {
		log.Println("Failed to send data (", conn.id, "): ", err)
	} else if l != len(data) {
		log.Println("Failed to send all data (", conn.id, ")")
	}
	// TODO: revisit this to find how to deal with errors!!
	return nil
}
