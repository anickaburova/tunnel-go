package main

import "sync/atomic"

var availableId uint32

func GetAvailableConnectionID() uint32 {
	return atomic.AddUint32(&availableId, 1)
}
