package main

type DisconnectMonitor interface {
	ReportDisconnect(uint32)
}
