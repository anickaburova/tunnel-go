package main

import (
	"io/ioutil"
	"os"
	"time"
)

type FileTunnel struct {
	fileIn  string
	fileOut string
	wait    time.Duration
}

func (ft *FileTunnel) Send(data []byte) error {
	return ioutil.WriteFile(ft.fileOut, data, 0)
}
func (ft *FileTunnel) SetCallback(callback TunnelCallback) {
	go func() {
		var lastTime time.Time
		for {
			fi, err := os.Stat(ft.fileIn)
			if err != nil {
				goto wait
			}
			if thisTime := fi.ModTime(); lastTime.Before(thisTime) {
				data, err := ioutil.ReadFile(ft.fileIn)
				if err != nil {
					goto wait
				}
				lastTime = thisTime
				callback(data)
			}
		wait:
			time.Sleep(ft.wait)
		}
	}()
}
