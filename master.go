package main

import (
	"fmt"
	"log"
	"strconv"
	"sync"

	uuid "github.com/nu7hatch/gouuid"
)

type Master struct {
	tunnel            TunnelMgr
	servers           map[uint16]*Server
	connections       map[uint32]*Connection
	connectionsLocker sync.Mutex
	masterID          *uuid.UUID
	slaveID           *uuid.UUID
}

func createMaster() *Master {
	var master Master
	master.tunnel.Start(&master, "exchange/tunnel.in", "exchange/tunnel.out", true)
	if id, err := uuid.NewV4(); err != nil {
		log.Fatal(err)
	} else {
		master.masterID = id
		master.tunnel.sendData(nil)
		log.Println("My id:", *id)
		//ProcessHandshake(&master, HSVoid, nil, nil)
	}
	master.servers = make(map[uint16]*Server)
	master.connections = make(map[uint32]*Connection)
	return &master
}

func (master *Master) SetOther(id *uuid.UUID) {
	master.slaveID = id
	master.resetConnection()
}

func (master *Master) MyID() *uuid.UUID {
	return master.masterID
}

func (master *Master) OtherID() *uuid.UUID {
	return master.slaveID
}

func (master *Master) resetConnection() {
	if len(master.connections) > 0 {
		master.connectionsLocker.Lock()
		defer master.connectionsLocker.Unlock()
		if len(master.connections) > 0 {
			log.Println("Closing existing connections!")
			for _, conn := range master.connections {
				conn.Disconnect()
			}
			master.connections = make(map[uint32]*Connection)
		}
	}
}

func (master *Master) CreateConnection(args []string) error {
	if len(args) != 3 {
		return fmt.Errorf("connect requires 2 args, address and listening port")
	}
	address := args[1]
	_port, err := strconv.Atoi(args[2])
	if err != nil {
		return fmt.Errorf("connect requires the second argument to be port number: %v", err)
	}
	port := uint16(_port)
	log.Println(address, ":", port)
	// creates server, which will wait for the connection
	// if there is already a server listening on the port, than if there are no active connections, change the address
	// otherwise fail to create a new connection
	server, exists := master.servers[port]
	if exists {
		server.Lock()
		defer server.Unlock()
		if server.HasActiveConnections() {
			server.ChangeAddress(address)
		} else {
			return fmt.Errorf("Failed to create a new connection on the port %d, because there are active connection already", port)
		}
	} else {
		master.servers[port] = CreateServer(master, address, port, master.tunnel.InputMsgs)
	}
	return nil
}

// Connect will create a new connection
func (master *Master) Connect(conID uint32, address string) error {
	log.Printf("Connection %d has connected to '%s'", conID, address)
	return nil
}

// Disconnect will disconnect the connection or notify about it
func (master *Master) Disconnect(connID uint32) error {
	master.connectionsLocker.Lock()
	conn, exists := master.connections[connID]
	master.connectionsLocker.Unlock()
	if !exists {
		log.Println("Error: disconnect message to not existing connection")
		return nil
	}
	return conn.Disconnect()
}

// Data transfers the data from the other side
func (master *Master) Data(connID uint32, data []byte) error {
	master.connectionsLocker.Lock()
	conn, exists := master.connections[connID]
	master.connectionsLocker.Unlock()
	if !exists {
		log.Println("Error: data message to not existing connection")
		return nil
	} else {
		return conn.Data(data)
	}
}

func (master *Master) ReportDisconnect(connID uint32) {
	master.connectionsLocker.Lock()
	defer master.connectionsLocker.Unlock()
	delete(master.connections, connID)
}

func (master *Master) NewConnection(connection *Connection) {
	connection.AddDisconnectMonitor(master)
	id := connection.ID()
	master.connectionsLocker.Lock()
	defer master.connectionsLocker.Unlock()
	master.connections[id] = connection
}
