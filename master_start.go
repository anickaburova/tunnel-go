package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"strings"

	"github.com/chzyer/readline"
)

// Function constructor - constructs new function for listing given directory
func listFiles(path string) func(string) []string {
	return func(line string) []string {
		names := make([]string, 0)
		files, _ := ioutil.ReadDir(path)
		for _, f := range files {
			names = append(names, f.Name())
		}
		return names
	}
}

var completer = readline.NewPrefixCompleter(
	readline.PcItem("mode",
		readline.PcItem("vi"),
		readline.PcItem("emacs"),
	),
	readline.PcItem("quit"),
	readline.PcItem("connect",
		readline.PcItem("localhost:4446"),
	),
	//readline.PcItem("say",
	//readline.PcItemDynamic(listFiles("./"),
	//readline.PcItem("with",
	//readline.PcItem("following"),
	//readline.PcItem("items"),
	//),
	//),
	//readline.PcItem("hello"),
	//readline.PcItem("bye"),
	//),
	//readline.PcItem("setprompt"),
	//readline.PcItem("setpassword"),
	//readline.PcItem("bye"),
	//readline.PcItem("help"),
	//readline.PcItem("go",
	//readline.PcItem("build", readline.PcItem("-o"), readline.PcItem("-v")),
	//readline.PcItem("install",
	//readline.PcItem("-v"),
	//readline.PcItem("-vv"),
	//readline.PcItem("-vvv"),
	//),
	//readline.PcItem("test"),
	//),
	//readline.PcItem("sleep"),
)

func filterInput(r rune) (rune, bool) {
	switch r {
	// block CtrlZ feature
	case readline.CharCtrlZ:
		return r, false
	}
	return r, true
}

func startMaster() {
	l, err := readline.NewEx(&readline.Config{
		Prompt:          "\033[31m»\033[0m ",
		HistoryFile:     "/tmp/tunnel-go.tmp",
		AutoComplete:    completer,
		InterruptPrompt: "^C",
		EOFPrompt:       "bye bye",

		HistorySearchFold:   true,
		FuncFilterInputRune: filterInput,
	})
	if err != nil {
		panic(err)
	}

	defer l.Close()

	cmds := make(chan []string)
	results := make(chan error)
	go run_master(cmds, results)
	l.SetVimMode(true)

	for {
		line, err := l.Readline()
		if err == readline.ErrInterrupt {
			if len(line) == 0 {
				break
			} else {
				continue
			}
		} else if err == io.EOF {
			break
		}
		line = strings.TrimSpace(line)

		quit := false
		args := strings.Fields(line)
		if len(args) == 0 {
			continue
		}
		switch args[0] {
		case "quit":
			quit = true
		default:
			cmds <- args
			err := <-results
			if err != nil {
				log.Println(err)
			}
		}
		if quit {
			break
		}
	}
}

func run_master(cmds chan []string, results chan error) {
	master := createMaster()
	for args := range cmds {
		switch args[0] {
		case "connect":
			results <- master.CreateConnection(args)
		default:
			results <- fmt.Errorf("Huh?")
		}
	}
}
