package main

import (
	"encoding/binary"
	"io"
)

const (
	HEADER_SIZE uint32 = 8
)

type MsgID uint32

const (
	MIDError      MsgID = iota
	MIDHandshake        // handshake message
	MIDSync             // synchronise tunnel reading, so they dont send all the messages constantly
	MIDConnect          // tell the slave to start connection to an address
	MIDDisconnect       // tell the slave, or master the connection has been lost
	MIDData             // send data over the tunnel
)

type Msg interface {
	ID() MsgID
	Send(io.Writer) error
	Execute(*TunnelMgr) error
}

func sendID(msg Msg, writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, uint32(msg.ID()))
}

func alwaysCheck(msg Msg) bool {
	switch msg.ID() {
	case MIDHandshake:
		return true
	default:
		return false
	}
}
