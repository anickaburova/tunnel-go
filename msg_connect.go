package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

var connect_create int = func() int {
	msgFactorySingleton.add(MIDConnect, func(data []byte) (msg Msg, rest []byte, err error) {
		data = data[8:] // skip msgid and size
		conID := binary.BigEndian.Uint32(data)
		address, rest, e := readString(data[4:])
		if e != nil {
			err = e
			return
		}

		msg = &MsgConnect{conID, address}
		return
	})
	return 0
}()

type MsgConnect struct {
	connectionID uint32
	address      string
}

func (m *MsgConnect) ID() MsgID {
	return MIDConnect
}

func (m *MsgConnect) Send(writer io.Writer) error {
	if err := sendID(m, writer); err != nil {
		return err
	}
	size := 16 + len(m.address)
	if err := binary.Write(writer, binary.BigEndian, uint32(size)); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, m.connectionID); err != nil {
		return err
	}
	_, err := writeString(writer, m.address)
	return err
}

func (m *MsgConnect) Execute(tunnel *TunnelMgr) error {
	return tunnel.App.Connect(m.connectionID, m.address)
}

func (m MsgConnect) String() string {
	var buffer bytes.Buffer
	fmt.Fprintf(&buffer, "Connect %d to %s", m.connectionID, m.address)
	return buffer.String()
}
