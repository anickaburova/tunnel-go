package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

var dataCreate int = func() int {
	msgFactorySingleton.add(MIDData, func(data []byte) (msg Msg, rest []byte, err error) {
		msgSize := binary.BigEndian.Uint32(data[4:8]) - 12
		data = data[8:] // skip msgid and size
		conID := binary.BigEndian.Uint32(data)
		msgData := data[4 : 4+msgSize]
		msg = &MsgData{conID, msgData}
		rest = data[4+msgSize:]
		return
	})
	return 0
}()

type MsgData struct {
	connectionID uint32
	data         []byte
}

func (m *MsgData) ID() MsgID {
	return MIDData
}

func (m *MsgData) Send(writer io.Writer) error {
	if err := sendID(m, writer); err != nil {
		return err
	}
	size := 12 + len(m.data)
	if err := binary.Write(writer, binary.BigEndian, uint32(size)); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, m.connectionID); err != nil {
		return err
	}

	if n, err := writer.Write(m.data); err != nil {
		return err
	} else if n != len(m.data) {
		return fmt.Errorf("Failed to write all data to the writer")
	}
	return nil
}

func (m *MsgData) Execute(tunnel *TunnelMgr) error {
	return tunnel.App.Data(m.connectionID, m.data)
}

func (m MsgData) String() string {
	var buffer bytes.Buffer
	fmt.Fprint(&buffer, "Data at ", m.connectionID, ": ", string(m.data))
	return buffer.String()
}
