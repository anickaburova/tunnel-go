package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

var disconnect_create int = func() int {
	msgFactorySingleton.add(MIDDisconnect, func(data []byte) (msg Msg, rest []byte, err error) {
		data = data[8:] // skip msgid and size
		conID := binary.BigEndian.Uint32(data)
		msg = &MsgDisconnect{conID}
		return
	})
	return 0
}()

type MsgDisconnect struct {
	connectionID uint32
}

func (m *MsgDisconnect) ID() MsgID {
	return MIDDisconnect
}

func (m *MsgDisconnect) Send(writer io.Writer) error {
	if err := sendID(m, writer); err != nil {
		return err
	}
	size := 12
	if err := binary.Write(writer, binary.BigEndian, uint32(size)); err != nil {
		return err
	}
	return binary.Write(writer, binary.BigEndian, m.connectionID)

}

func (m *MsgDisconnect) Execute(tunnel *TunnelMgr) error {
	return tunnel.App.Disconnect(m.connectionID)
}

func (m MsgDisconnect) String() string {
	var buffer bytes.Buffer
	fmt.Fprint(&buffer, "Disconnect ", m.connectionID)
	return buffer.String()
}
