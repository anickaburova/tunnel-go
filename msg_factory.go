package main

import (
	"encoding/binary"
	"log"
)

type MsgParse (func([]byte) (Msg, []byte, error))

var msgFactorySingleton msgFactory = msgFactory{make(map[MsgID]MsgParse)}

type msgFactory struct {
	msg map[MsgID]MsgParse
}

func (f *msgFactory) add(id MsgID, factory MsgParse) error {
	f.msg[id] = factory
	return nil
}

func (f *msgFactory) parse(data []byte) (index uint32, msg Msg, rest []byte, err error) {
	//log.Println("Parsing data", data)
	index = binary.BigEndian.Uint32(data)
	id := MsgID(binary.BigEndian.Uint32(data[4:8]))
	fac, ok := f.msg[id]
	if !ok {
		log.Printf("Error: Unknown message: %v", id)
		goto setrest
	} else {
		msg, rest, err = fac(data[4:])
		//log.Println("Parse:", data, rest)
		if err != nil {
			goto setrest
		}
	}
	return
setrest:
	size := int(binary.BigEndian.Uint32(data))
	if size < len(data) {
		rest = data[size:]
	}
	return
}
