package main

import (
	"encoding/binary"
	"fmt"
	"io"
	"log"
)

type queueItem Msg

type msgQueue struct {
	data    []queueItem
	begin   int
	end     int // the first empty
	idStart int
	id      int
}

func CreateMsgQueue(capacity int) msgQueue {
	var queue msgQueue
	queue.data = make([]queueItem, capacity, capacity)
	queue.begin = 0
	queue.end = 0
	queue.idStart = 1 // start from 1, so tunnel manager can easily track the start
	queue.id = 1
	return queue
}

func (queue *msgQueue) Reset() {
	queue.begin = 0
	queue.end = 0
	queue.idStart = 1 // start from 1, so tunnel manager can easily track the start
	queue.id = 1
}

func (queue *msgQueue) Any() bool {
	return queue.idStart != queue.id
}

func (queue *msgQueue) Size() int {
	if queue.end >= queue.begin {
		return queue.end - queue.begin
	} else {
		capacity := cap(queue.data)
		return capacity - queue.begin + queue.end
	}
}

func (queue *msgQueue) Send(writer io.Writer) error {
	log.Println(queue.begin, queue.end)
	if queue.end >= queue.begin {
		for i, index := queue.begin, queue.idStart; i < queue.end; i, index = i+1, index+1 {
			if err := binary.Write(writer, binary.BigEndian, uint32(index)); err != nil {
				return err
			}
			log.Println(index, queue.data[i])
			if err := queue.data[i].Send(writer); err != nil {
				return err
			}
		}
	} else {
		index := queue.idStart
		capacity := cap(queue.data)
		for i := queue.begin; i < capacity; i++ {
			if err := binary.Write(writer, binary.BigEndian, uint32(index)); err != nil {
				return err
			}
			log.Println(index, queue.data[i])
			if err := queue.data[i].Send(writer); err != nil {
				return err
			}
			index++
		}
		for i := 0; i < queue.end; i++ {
			if err := binary.Write(writer, binary.BigEndian, uint32(index)); err != nil {
				return err
			}
			log.Println(index, queue.data[i])
			if err := queue.data[i].Send(writer); err != nil {
				return err
			}
			index++
		}
	}
	return nil
}

func (queue *msgQueue) Get(index int) (id int, msg queueItem, err error) {
	size := queue.Size()
	if index >= size {
		err = fmt.Errorf("Index (%d) is out of range (%d)", index, size)
		return
	}
	offset := index + queue.begin
	capacity := cap(queue.data)
	if offset >= capacity {
		offset -= capacity
	}
	msg = queue.data[offset]
	id = queue.idStart + index
	return
}

func (queue *msgQueue) Add(msg queueItem) {
	if msg == nil {
		log.Fatal("Adding nil item!")
	}
	capacity := cap(queue.data)
	// check if there is space
	var size int
	if queue.end >= queue.begin {
		size = queue.end - queue.begin
	} else {
		size = capacity - queue.begin + queue.end
	}
	if size == capacity {
		queue.expand()
	} else if queue.end == capacity {
		queue.end = 0
	}
	queue.data[queue.end] = msg
	queue.end++
	queue.id++
}

func (queue *msgQueue) expand() {
	capacity := cap(queue.data)
	data := make([]queueItem, capacity*2, capacity*2)
	if queue.end >= queue.begin {
		for i := queue.begin; i < queue.end; i++ {
			data[i-queue.begin] = queue.data[i]
		}
	} else {
		for i := queue.begin; i < capacity; i++ {
			data[i-queue.begin] = queue.data[i]
		}
		for i := 0; i < queue.end; i++ {
			data[i+capacity-queue.begin] = queue.data[i]
		}
	}
	queue.begin = 0
	queue.end = capacity
	queue.data = data
}

func (queue *msgQueue) Sync(id int) {
	log.Println("Queue sync", id)
	// special case if syncing fully
	if id == queue.id-1 {
		queue.begin = 0
		queue.end = 0
	} else {
		if queue.end >= queue.begin {
			beginId := queue.id - (queue.end - queue.begin)
			if id < beginId {
				return // nothing to sync, it is already behind the sync id
			}
			toRemove := id - beginId + 1
			queue.begin += toRemove
		} else {
			capacity := cap(queue.data)
			beginId := queue.id - (capacity - queue.begin + queue.end)
			if id < beginId {
				return // nothing to sync, it is already behind the sync id
			}
			toRemove := id - beginId + 1
			queue.begin += toRemove
			if queue.begin >= capacity {
				queue.begin -= capacity
			}
		}
	}
	queue.idStart = id
}
