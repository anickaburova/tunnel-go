package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

var sync_create int = func() int {
	msgFactorySingleton.add(MIDSync, func(data []byte) (msg Msg, rest []byte, err error) {
		data = data[8:] // skip msgid and size
		id := binary.BigEndian.Uint32(data)
		msg = &MsgSync{id}
		rest = data[4:]
		return
	})
	return 0
}()

type MsgSync struct {
	id uint32
}

func (m *MsgSync) ID() MsgID {
	return MIDSync
}

func (m *MsgSync) Send(writer io.Writer) error {
	if err := sendID(m, writer); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, uint32(12)); err != nil {
		return err
	}
	return binary.Write(writer, binary.BigEndian, uint32(m.id))
}

func (m *MsgSync) Execute(tunnel *TunnelMgr) error {
	return tunnel.sync(m.id)
}

func (m MsgSync) String() string {
	var buffer bytes.Buffer
	fmt.Fprint(&buffer, "Sync at ", m.id)
	return buffer.String()
}
