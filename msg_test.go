package main

import (
	"bytes"
	"encoding/binary"
	"reflect"
	"testing"
)

func TestSync(t *testing.T) {
	msg := MsgSync{33}
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.BigEndian, uint32(1234567))
	msg.Send(buffer)
	index, out, _, err := msgFactorySingleton.parse(buffer.Bytes())
	if err != nil {
		t.Error(err)
	}
	if index != 1234567 {
		t.Error("Index is incorrect: ", index)
	}
	m, ok := out.(*MsgSync)
	if !ok {
		t.Error("Deserialised is not Sync")
	}
	if *m != msg {
		t.Error("Sync not same data: ", *m, " != ", msg)
	}
}

func TestConnect(t *testing.T) {
	msg := MsgConnect{33, "localhost:3323"}
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.BigEndian, uint32(1234567))
	msg.Send(buffer)
	index, out, _, err := msgFactorySingleton.parse(buffer.Bytes())
	if err != nil {
		t.Error(err)
	}
	if index != 1234567 {
		t.Error("Index is incorrect: ", index)
	}
	m, ok := out.(*MsgConnect)
	if !ok {
		t.Error("Deserialised is not Connect")
	}
	if *m != msg {
		t.Error("Connect not same data: ", *m, " != ", msg)
	}
}

func TestDisconnect(t *testing.T) {
	msg := MsgDisconnect{22}
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.BigEndian, uint32(1234567))
	msg.Send(buffer)
	index, out, _, err := msgFactorySingleton.parse(buffer.Bytes())
	if err != nil {
		t.Error(err)
	}
	if index != 1234567 {
		t.Error("Index is incorrect: ", index)
	}
	m, ok := out.(*MsgDisconnect)
	if !ok {
		t.Error("Deserialised is not Disconnect")
	}
	if *m != msg {
		t.Error("Disconnect not same data: ", *m, " != ", msg)
	}
}

func TestData(t *testing.T) {
	msg := MsgData{22, []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}}
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.BigEndian, uint32(1234567))
	msg.Send(buffer)
	index, out, _, err := msgFactorySingleton.parse(buffer.Bytes())
	if err != nil {
		t.Error(err)
	}
	if index != 1234567 {
		t.Error("Index is incorrect: ", index)
	}
	m, ok := out.(*MsgData)
	if !ok {
		t.Error("Deserialised is not Data")
	}
	if !reflect.DeepEqual(*m, msg) {
		t.Error("Data not same data: ", *m, " != ", msg)
	}
}
