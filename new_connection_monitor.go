package main

type NewConnectionMonitor interface {
	NewConnection(*Connection)
}
