package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"reflect"
	"testing"

	"gitlab.com/anickaburova/xxd"
)

func TestQueueSerial(t *testing.T) {
	queue := CreateMsgQueue(2)
	queue.Add(&MsgConnect{1, "localhost:3333"})
	queue.Add(&MsgData{1, []byte("hello world")})
	queue.Add(&MsgSync{4})
	queue.Add(&MsgData{1, []byte("hello world again:)")})
	queue.Add(&MsgDisconnect{1})

	buffer := new(bytes.Buffer)
	if err := queue.Send(buffer); err != nil {
		t.Error(err)
	}

	data := buffer.Bytes()
	format := xxd.DefaultFormat()
	output := bufio.NewWriter(os.Stdout)
	format.Xxd(buffer, output)
	output.Flush()

	i := 0
	for len(data) > 0 {
		index, msg, next, err := msgFactorySingleton.parse(data)
		if err != nil {
			t.Error(err)
		}
		if origIndex, orig, err := queue.Get(i); err != nil {
			t.Error(err)
		} else {
			if origIndex != int(index) {
				t.Error("Indexes are not matching: ", origIndex, " != ", index)
			}
			if !reflect.DeepEqual(orig, msg) {
				t.Error("Data not same data: ", msg, " != ", orig)
			}
		}
		fmt.Println(index, ": ", msg)
		data = next
		i++
	}
}
