package main

import (
	"fmt"
	"log"
	"net"
	"sync"
)

type Server struct {
	onNewConnection   NewConnectionMonitor
	locker            sync.Mutex
	address           string
	port              uint16
	connections       map[uint32]*Connection
	connectionsLocker sync.Mutex
	msgQueue          chan Msg
}

func CreateServer(onNewConnection NewConnectionMonitor, address string, port uint16, msgQueue chan Msg) *Server {
	var server Server
	server.onNewConnection = onNewConnection
	server.address = address
	server.port = port
	server.msgQueue = msgQueue
	server.connections = make(map[uint32]*Connection)
	go func() {
		netaddr := fmt.Sprintf("0.0.0.0:%d", port)
		l, err := net.Listen("tcp", netaddr)
		if err != nil {
			log.Fatal(err)
		}
		defer l.Close()
		for {
			log.Println("Listening on ", port, " for connect to: ", address)
			conn, err := l.Accept()
			if err != nil {
				log.Fatal(err)
			}
			server.createConnection(conn)
		}
	}()
	return &server
}

func (server *Server) Lock() {
	server.locker.Lock()
}

func (server *Server) Unlock() {
	server.locker.Unlock()
}

func (server *Server) HasActiveConnections() bool {
	return len(server.connections) > 0
}

func (server *Server) ChangeAddress(address string) {
	server.Lock()
	defer server.Unlock()
	server.address = address
}

func (server *Server) createConnection(conn net.Conn) {
	log.Printf("Got connection for '%s'", server.address)
	connection := CreateConnection(server.address, conn, server.msgQueue, nil)
	connection.AddDisconnectMonitor(server)
	server.onNewConnection.NewConnection(connection)
	id := connection.ID()
	server.connectionsLocker.Lock()
	defer server.connectionsLocker.Unlock()
	server.connections[id] = connection
}

func (server *Server) ReportDisconnect(connID uint32) {
	server.connectionsLocker.Lock()
	defer server.connectionsLocker.Unlock()
	delete(server.connections, connID)
}
