package main

import (
	"log"
	"sync"
	"time"

	uuid "github.com/nu7hatch/gouuid"
)

type Slave struct {
	tunnel            TunnelMgr
	connections       map[uint32]*Connection
	connectionsLocker sync.Mutex
	slaveID           *uuid.UUID
	masterID          *uuid.UUID
}

func startSlave() {
	var slave Slave
	slave.connections = make(map[uint32]*Connection)
	slave.tunnel.Start(&slave, "exchange/tunnel.out", "exchange/tunnel.in", true)
	//time.Sleep(time.Second * 2) // wait until we have messages
	if id, err := uuid.NewV4(); err != nil {
		log.Fatal(err)
	} else {
		slave.slaveID = id
		slave.tunnel.sendData(nil)
		log.Println("My id:", *id)
		//ProcessHandshake(&slave, HSVoid, nil, nil)
	}
	log.Println("Slave started")
	for {
		time.Sleep(time.Minute)
	}
}

func (slave *Slave) SetOther(id *uuid.UUID) {
	slave.masterID = id
	slave.resetConnection()
}

func (slave *Slave) MyID() *uuid.UUID {
	return slave.slaveID
}

func (slave *Slave) OtherID() *uuid.UUID {
	return slave.masterID
}

func (slave *Slave) resetConnection() {
	if len(slave.connections) > 0 {
		slave.connectionsLocker.Lock()
		defer slave.connectionsLocker.Unlock()
		if len(slave.connections) > 0 {
			log.Println("Closing existing connections!")
			for _, conn := range slave.connections {
				conn.Disconnect()
			}
			slave.connections = make(map[uint32]*Connection)
		}
	}
}

// Connect will create a new connection
func (slave *Slave) Connect(connectionID uint32, address string) error {
	connection, err := CreateClient(connectionID, address, slave.tunnel.InputMsgs)
	if err != nil {
		return err
	}
	connection.AddDisconnectMonitor(slave)
	id := connection.ID()
	slave.connectionsLocker.Lock()
	defer slave.connectionsLocker.Unlock()
	slave.connections[id] = connection
	return nil
}

// Disconnect will disconnect the connection or notify about it
func (slave *Slave) Disconnect(connID uint32) error {
	slave.connectionsLocker.Lock()
	conn, exists := slave.connections[connID]
	slave.connectionsLocker.Unlock()
	if !exists {
		log.Println("Error: disconnect message to not existing connection")
		return nil
	}
	return conn.Disconnect()
}

// Data transfers the data from the other side
func (slave *Slave) Data(connID uint32, data []byte) error {
	slave.connectionsLocker.Lock()
	conn, exists := slave.connections[connID]
	slave.connectionsLocker.Unlock()
	if !exists {
		log.Println("Error: data message to not existing connection")
		return nil
	} else {
		return conn.Data(data)
	}
}

func (slave *Slave) ReportDisconnect(connID uint32) {
	slave.connectionsLocker.Lock()
	defer slave.connectionsLocker.Unlock()
	delete(slave.connections, connID)
}

//func (slave *Slave) NewConnection(connection *Connection) {
//connection.AddDisconnectMonitor(slave)
//id := connection.ID()
//slave.connectionsLocker.Lock()
//defer slave.connectionsLocker.Unlock()
//slave.connections[id] = connection
//}

//func (server *Server) createConnection(conn net.Conn) {
//log.Printf("Got connection for '%s'", server.address)
//connection := CreateConnection(server.address, conn, server.msgQueue)
//connection.AddDisconnectMonitor(server)
//server.onNewConnection.NewConnection(connection)
//id := connection.ID()
//server.connectionsLocker.Lock()
//defer server.connectionsLocker.Unlock()
//server.connections[id] = connection
//}
