package main

import (
	"fmt"
	"log"
	"os"

	"github.com/juju/gnuflag"
)

func main() {
	log.SetFlags(log.Lshortfile)
	version := gnuflag.Bool("version", false, "Prints the current version")
	gnuflag.Parse(true)
	if *version {
		fmt.Println(PackageVersion)
		os.Exit(0)
	}

	if len(gnuflag.Args()) < 1 {
		fmt.Fprintln(os.Stderr, "Missing tunnel mode")
		os.Exit(1)
	}

	switch gnuflag.Arg(0) {
	case "master":
		startMaster()
	case "slave":
		startSlave()
	}
}
