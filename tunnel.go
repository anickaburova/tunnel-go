package main

type TunnelCallback (func([]byte))

// Tunnel writes and reads data from the whatever is using for communication
type Tunnel interface {
	Send([]byte) error
	SetCallback(TunnelCallback)
}
