package main

import (
	"bytes"
	"log"
	"reflect"
	"sync"

	uuid "github.com/nu7hatch/gouuid"
)

const UUID_SIZE = 16

// TunnelMgr manages connection to the tunnel and dispatching messages either way
type TunnelMgr struct {
	tunnel     Tunnel
	outputMsgs msgQueue
	App        App
	InputMsgs  chan Msg
	locker     sync.Mutex
	lastMsg    uint32
	syncCount  int
}

func (mgr *TunnelMgr) sync(id uint32) error {
	mgr.locker.Lock()
	defer mgr.locker.Unlock()
	log.Println("Syncing message:", id)
	mgr.outputMsgs.Sync(int(id))
	return nil
}

func (mgr *TunnelMgr) Reset() {
	log.Println("Reseting queue")
	mgr.locker.Lock()
	defer mgr.locker.Unlock()
	mgr.outputMsgs.Reset()
	mgr.lastMsg = 0
}

func (mgr *TunnelMgr) Start(app App, filein, fileout string, goroutine bool) error {
	mgr.App = app
	aws, err := CreateAWS(filein, fileout)
	if err != nil {
		log.Fatal(err)
	}
	mgr.tunnel = aws
	aws.SetCallback(func(data []byte) {
		mgr.processData(data)
	})
	mgr.outputMsgs = CreateMsgQueue(16)
	mgr.InputMsgs = make(chan Msg)
	main := func() {
		log.Println("Internal tunnel started input loop")
		for msg := range mgr.InputMsgs {
			//log.Println("Tunnel received message: ", msg)
			mgr.sendData(msg)
		}
		log.Println("Internal tunnel ended input loop")
	}
	if goroutine {
		go main()
	} else {
		main()
	}
	return nil
}

func (mgr *TunnelMgr) addMsg(msg Msg) {
	mgr.locker.Lock()
	defer mgr.locker.Unlock()
	mgr.outputMsgs.Add(msg)
}

func (mgr *TunnelMgr) sendData(msg Msg) {
	mgr.locker.Lock()
	if msg != nil {
		mgr.outputMsgs.Add(msg)
	}
	writer := new(bytes.Buffer)
	// write the UUIDs
	if n, err := writer.Write((*mgr.App.MyID())[:]); err != nil {
		log.Fatal(err)
	} else if n != int(UUID_SIZE) {
		log.Fatal("Failed to write all data to the writer")
	}
	var buffer []byte
	if mgr.App.OtherID() != nil {
		buffer = (*mgr.App.OtherID())[:]
	} else {
		buffer = make([]byte, UUID_SIZE)
	}
	if n, err := writer.Write(buffer); err != nil {
		log.Fatal(err)
	} else if n != int(UUID_SIZE) {
		log.Fatal("Failed to write all data to the writer")
	}
	if err := mgr.outputMsgs.Send(writer); err != nil {
		log.Fatal(err)
	}
	mgr.locker.Unlock()
	data := writer.Bytes()
	mgr.tunnel.Send(data)
}

func (mgr *TunnelMgr) processData(data []byte) {
	//defer func() {
	//if r := recover(); r != nil {
	//log.Println("Processing data fatal error:", r)
	//}
	//}()
	log.Println("Processing data!")
	var lastMsg uint32 = 0
	// the first 16 bytes should be ID of the sender
	// the other 16 bytes is us, or 0 or somebody else
	if len(data) < int(UUID_SIZE*2) {
		// not enough data received
		return
	}
	sender, err := uuid.Parse(data[:UUID_SIZE])
	if err != nil {
		return
	}
	if !isSameID(sender, mgr.App.OtherID()) {
		// we need to resend data, because the there is a new other
		log.Println("Getting a new other:", *sender)
		mgr.App.SetOther(sender)
		mgr.Reset()
		mgr.sendData(nil)
	}
	data = data[UUID_SIZE:]
	receiver, err := uuid.Parse(data[:UUID_SIZE])
	if err != nil {
		return
	}
	if !isSameID(receiver, mgr.App.MyID()) {
		// this is not for us!!!
		log.Println("This is not for us!!:", *receiver)
		return
	}
	log.Println("Data for us")
	data = data[UUID_SIZE:]
	for len(data) > 0 {
		msgID, msg, next, err := msgFactorySingleton.parse(data)
		if err != nil {
			log.Fatal("Error: Message parse failed: ", err)
		}
		if msgID > mgr.lastMsg {
			log.Println("Got msg: ", msgID, msg)
			if err = msg.Execute(mgr); err != nil {
				log.Fatal("Error: Message failed to execute: ", err)
			}
			if msg.ID() == MIDSync {
				mgr.syncCount++
			} else {
				mgr.syncCount = 0
			}
		}
		//log.Println(data, next)
		lastMsg = msgID
		data = next
	}
	// we read all this messages, we can send sync back to tunnel
	log.Println("Processsing finished:", lastMsg, mgr.lastMsg)
	if lastMsg > mgr.lastMsg {
		if mgr.syncCount > 1 {
			return
		}
		log.Println("Syncing messages")
		mgr.lastMsg = lastMsg
		mgr.InputMsgs <- &MsgSync{lastMsg}
	}
}

func isSameID(left *uuid.UUID, right *uuid.UUID) bool {
	if left == right {
		return true
	}
	if left == nil || right == nil {
		return false
	}
	return reflect.DeepEqual(*left, *right)
}
