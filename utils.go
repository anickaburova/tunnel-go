package main

import (
	"encoding/binary"
	"fmt"
	"io"
)

func writeString(writer io.Writer, text string) (length int, err error) {
	if err = binary.Write(writer, binary.BigEndian, uint32(len(text))); err != nil {
		return
	}
	if err = binary.Write(writer, binary.BigEndian, []byte(text)); err != nil {
		return
	}
	length = 4 + len(text)
	return
}

func readString(data []byte) (result string, rest []byte, err error) {
	l := uint32(len(data))
	if l < 4 {
		err = fmt.Errorf("Not enough data to read size of the string")
		return
	}
	size := binary.BigEndian.Uint32(data[:4])
	if l-4 < size {
		err = fmt.Errorf("Not enough data in the reader to read full string (%d != %d)", size, l-4)
		return
	}
	result = string(data[4 : size+4])
	rest = data[size+4:]
	return
}
